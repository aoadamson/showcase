from flask import (
    Flask,
    render_template,
    request
)
from methods.get_photos import get_photos

app = Flask(__name__)


@app.route('/')
def student():
    return render_template('index.html')


@app.route('/result', methods=['POST'])
def result():
    if request.method == 'POST':
        response = get_photos(request.form["Album Id"])
        if not response:
            return render_template(
                'bad_request.html'
            ), 400
        else:
            return render_template(
                'result.html',
                response=response,
            )


if __name__ == "__main__":
    app.run(debug=True)
