# Album Lookup

## Windows Setup:

```sh
  > git clone https://aoadamson@bitbucket.org/aoadamson/showcase.git
  > pip install -r requirements.txt
  > set FLASK_APP=app.py
  > python -m flask run
```

## Linux Setup:

```sh
  $ git clone https://aoadamson@bitbucket.org/aoadamson/showcase.git
  $ pip install -r requirements.txt
  $ export FLASK_APP=app.py
  $ python -m flask run
```
## How to use:

-  Enter which album you number you are looking for and it will present you with its name and thumbnail.
