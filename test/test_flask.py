from app import app
import unittest


class TestMyApp(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()

    def test_main(self):
        rv = self.app.get('/')
        assert rv.status == '200 OK'

    def test_404(self):
        rv = self.app.get('/invalid')
        self.assertEqual(rv.status, '404 NOT FOUND')

    def test_good_request(self):
        rv = self.app.post('/result', data={"Album Id": 2})
        self.assertEqual(rv.status, '200 OK')

    def test_bad_request(self):
        rv = self.app.post('/result', data={"Album Id": "f"})
        self.assertEqual(rv.status, '400 BAD REQUEST')
