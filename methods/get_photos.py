import requests


def get_photos(album_id):
    """
    :param album_id:
    :return json object of the response:
    """
    url = "https://jsonplaceholder.typicode.com/photos"
    response = requests.get(url, {"albumId": album_id})
    return response.json()
